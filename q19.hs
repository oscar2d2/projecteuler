--Problem 19
--Counting Sundays

isLeapYr :: Int -> Bool
isLeapYr y = y `mod` 4 == 0 && (y `mod` 100 /= 0 || (y `mod` 100 == 0 &&y `mod` 400 == 0))

year2Months :: Int -> [Int]
year2Months y
  | isLeapYr y = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  | otherwise = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

twentyCenturyMonths :: [Int]
twentyCenturyMonths = concat $ map year2Months [1901..2000]

accumMonths :: [Int]
accumMonths = init $ scanl (+) 1 twentyCenturyMonths

sol :: Int
sol = length $ filter (\x -> x `mod` 7 == 6) accumMonths

main = do
    print sol
