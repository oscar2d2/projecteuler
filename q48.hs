--Problem 48
--Self powers

sol :: Integer
sol = (sum $ map (\x -> x^x) [1..1000]) `mod` 10000000000

main = do
    print sol
