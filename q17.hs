d = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
dd = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
dd' = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

oneToNine = sum . map length $ d
tenToNineteen = sum . map length $ dd
twentyToNinetyNine = sum . map (\x -> (length x) * 10 + oneToNine) $ dd' 
oneHundredToNineHundredNinetyNine = sum .  map (\x -> ((length x) + (length "hundred")) * 100 + (length "and") * 99 + oneToNine + tenToNineteen + twentyToNinetyNine) $ d
oneThousand = length "oneThousand"

sol = oneToNine + tenToNineteen + twentyToNinetyNine + oneHundredToNineHundredNinetyNine + oneThousand
