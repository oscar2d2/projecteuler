--Problem 22
--Names scores

import Data.Char (ord)
import Data.List (sort)
import Data.List.Split (splitOn)
import System.IO

baseScore :: String -> Int -> Int
baseScore [] score = score
baseScore (x:xs) score = baseScore xs (s + score)
    where s = (ord x) - 64 -- (ord x) - (ord 'A') + 1

escapeDoubleQuote :: String -> String
escapeDoubleQuote [] = []
escapeDoubleQuote ('\"': xs) = escapeDoubleQuote xs
escapeDoubleQuote (x:xs) = x : escapeDoubleQuote xs

solve :: String -> Int 
solve str = sum $ zipWith (*) [1..length baseScores] baseScores
  where baseScores = map ((flip baseScore) 0) . sort . map escapeDoubleQuote . splitOn "," $ str

main = do
    contents <- readFile "p022_names.txt"
    print $ solve contents
