q9ans :: Int
q9ans = foldr (*) 1 $ head [[a,b, 1000 - a - b] | a <- [1..333], b <- [a+1..500], a^2 + b^2 == (1000 - a - b)^2]