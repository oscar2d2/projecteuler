--Non-abundant sums
import Control.Applicative
import Data.List
import Data.MemoCombinators as Memo

properDivisors :: Integer -> [Integer]
properDivisors x = init . sort . nub . concat $ [[i, x `div` i] | i <- [1..toInteger . floor . sqrt . fromIntegral $ x], x `mod` i == 0]

isAbundant :: Integer -> Bool
isAbundant x = (sum . properDivisors $ x) > x

allAbundant :: Integer -> [Integer]
allAbundant n = filter isAbundant [1..n]

isSumOfTwoAbundant :: (Integer -> Bool) -> Integer -> Bool
isSumOfTwoAbundant f x = or [f i && f (x-i) | i <- [1..x `div` 2]]

solve n = sum . filter (not . isSumOfTwoAbundant isAbundant') $ [1..n]
    where isAbundant' = Memo.arrayRange(0, n) isAbundant

sol = solve 28123
