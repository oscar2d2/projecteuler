fib :: Int -> Integer
fib n = fibs !! n
  where
    fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

sumEvenFib :: Integer
sumEvenFib = sum $ filter even $ takeWhile (< 4000000) $ map fib [1..]
