q6ans :: Int
q6ans = (square $ sum [1..100]) - (sum $ map (^2) [1..100])
	where square x = x * x