import Data.List

intToList :: (Integral a) => a -> [a]
intToList n
  | n >= 1 && n <= 9 = [n]
  | otherwise = intToList (n `div` 10) ++ [n `mod` 10]

palindrome :: (Integral a) => [a] -> Bool
palindrome xs = (==) xs $ reverse xs

q4ans = head $ filter (palindrome) $ map intToList $ reverse $ sort [x * y | x<-[100..999], y<-[100..999]]
