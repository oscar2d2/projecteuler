sol :: Integer
sol = sum . takeWhile (<= 1001 * 1001) . map (+1) . scanl (+) 0 . concat . map (replicate 4) $ [2,4..]
