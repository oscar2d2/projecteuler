--Reciprocal cycles
--Ref: Full Reptend Prime

import Data.Numbers.Primes

sq x p = (x' * x') `mod` p
  where x' = x `mod` p

pow b d p
  | d == 0 = 1
  | even d = sq (pow b (d `div` 2) p) p
  | otherwise = ((sq (pow b (d `div` 2) p) p) * b) `mod` p

isFullReptend p = pow 10 (p-1) p == 1 && (all (==True) . map (\k -> pow 10 k p /= 1) $ [1..p-2])

sol = head . filter (isFullReptend) . reverse . takeWhile (<1000) $ primes
