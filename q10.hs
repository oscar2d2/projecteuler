primes = 2 : [n | n<-[3..], all ((> 0).rem n) $ takeWhile ((<= n).(^2)) primes]

q10ans = sum $ takeWhile (<2000000) primes
