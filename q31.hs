--Problem 31
--Coin sums

f :: Int -> [Int] -> Int
f 0 _ = 1
f _ [] = 0
f amt (c:cs)
  | amt >= c = (f amt cs) + (f (amt - c) (c:cs))
  | otherwise = f amt cs

sol :: Int
sol = f 200 [200, 100, 50, 20, 10, 5, 2, 1]

main = do
    print sol
