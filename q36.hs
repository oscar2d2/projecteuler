--Double-base palindromes

isPali [] = True
isPali [x] = True
isPali xs
  | h == t = isPali (init . tail $ xs)
  | otherwise = False
  where h = head xs
        t = last xs

decToBin' _ [] = []
decToBin' n (x:xs)
  | n >= x = 1 : decToBin' (n-x) xs
  | otherwise = 0 : decToBin' n xs

decToBin n = decToBin' n nums
           where nums = reverse . takeWhile (<= n) . map (2^) $ [0..]

isDoublePali n = isPali (decToBin n) && isPali (show n)

sol = sum . filter isDoublePali $ [1..999999]
