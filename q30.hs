--Digit fifth powers
--Narcissistic Number
import Data.Char

digitFifthPower :: Int -> Int
digitFifthPower = sum . map ((^5) . digitToInt) . show

is5Narcissistic :: Int -> Bool
is5Narcissistic x = x == (digitFifthPower x)

sol = sum . filter (is5Narcissistic) $ [2..(9^5)*5]
