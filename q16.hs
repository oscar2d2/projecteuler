input = 2^1000

sumDigit :: Integer -> Integer
sumDigit x
  | x < 10 = x
  | otherwise = (x `mod` 10) + (sumDigit $ x `div` 10)

q16ans = sumDigit input
