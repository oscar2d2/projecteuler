primeFactors n  = factor n primes
  where factor n (x:xs) | x * x > n = [n]
                        | n `mod` x /= 0 = factor n xs
                        | otherwise = x : factor (n `div` x) (x:xs)
        primes = 2 : filter ((==1). length. primeFactors) [3,5..]

q3ans = last $ primeFactors 600851475143
