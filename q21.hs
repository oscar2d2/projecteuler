--Problem 21
--Amicable numbers

d :: Int -> Int
d n = 1 + sum [ i + (n `div` i) | i <- [2..floor . sqrt . fromIntegral $ n], n `mod` i == 0]

amicable :: Int -> Int
amicable x
  | d(y) == x && x /= y = x
  | otherwise = 0
    where y = d(x)

sol :: Int
sol = sum . map amicable $ [1..9999]

main = do
    print sol
