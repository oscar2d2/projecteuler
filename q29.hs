--Problem 29
--Distinct powers
import Data.List

sol :: Int
sol = length . group . sort $ [a^b | a <- [2..100] , b <- [2..100]]

main = do
    print sol
