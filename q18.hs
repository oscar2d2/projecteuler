--Problem 18
--Maximum path sum I
import System.IO

readInt :: String -> Int
readInt = read

f :: [Int] -> [Int] -> [Int]
f [] _ = []
f _ [] = []
f (x:xs) (y1:y2:ys) = max (x + y1) (x + y2) : (f xs (y2:ys))

solve :: [[Int]] -> Int
solve = head . foldr1 (\x acc -> f x acc)

main = do
    contents <- readFile "p018_triangle.txt"
    print . solve . map (map readInt) . map words . lines $contents
