--Digit factorials
import Data.Char

fact' 0 acc = acc 
fact' 1 acc = acc 
fact' x acc = fact' (x-1) (acc * x)

fact x = fact' x 1

isCurious x = (sum . map (fact . digitToInt) . show $ x) == x

solve n = sum . filter isCurious $ [3..n]

sol = solve 100000 
